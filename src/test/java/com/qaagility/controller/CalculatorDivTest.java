package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorDivTest {

    @Test
    public void testDivideWithValidInputs() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(10, 2);
        assertEquals("Dividing 10 by 2 should be 5", 5, result);
    }

    @Test
    public void testDivideByZero() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(8, 0);
        assertEquals("Dividing by zero should return Integer.MAX_VALUE", Integer.MAX_VALUE, result);
    }

    @Test
    public void testDivideWithNegativeInputs() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(-15, 3);
        assertEquals("Dividing -15 by 3 should be -5", -5, result);
    }

    @Test
    public void testDivideByNegativeDivisor() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(20, -4);
        assertEquals("Dividing 20 by -4 should be -5", -5, result);
    }

    @Test
    public void testDivideWithLargeNumbers() {
        Calculator calculator = new Calculator();
        int result = calculator.divide(Integer.MAX_VALUE, 1);
        assertEquals("Dividing Integer.MAX_VALUE by 1 should be Integer.MAX_VALUE", Integer.MAX_VALUE, result);
    }
}

